const express = require('express');
const path = require('path');
const app = express();

// Set static folder
//app.use(express.static(path.join(__dirname, 'public')));

//app.get("/url", (req, res, next) => {
//    res.json(["Tony", "Lisa", "Michael", "Ginger", "Food"]);
//});

app.get("/hello", (req, res, next) => {
    res.json([`response: Hello ${req.query.message}`]);
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));